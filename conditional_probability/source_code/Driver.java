package wordcount.org.main;
import java.util.Date;
import java.util.Formatter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class Driver {

    public static void main(String[] args) throws Exception {
    	
    	Configuration conf = new Configuration();      

            @SuppressWarnings("deprecation")
			Job job = new Job(conf, "csw1");
            job.setJarByClass(Driver.class);
      
            job.setOutputKeyClass(Pair.class);
            job.setOutputValueClass(IntWritable.class);
          
            job.setMapperClass(WordMapper.class);
            job.setReducerClass(WordReducer.class);
            job.setPartitionerClass(WordPartitioner.class);
          
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);
          
            //local exec.
            //@SuppressWarnings("resource")
            //Formatter formatter = new Formatter();
            //String outpath = "Out"
            //+ formatter.format("%1$tm%1$td%1$tH%1$tM%1$tS", new Date());
            //FileInputFormat.setInputPaths(job, new Path("In"));
            //FileOutputFormat.setOutputPath(job, new Path(outpath));
            
            // aws
            FileInputFormat.addInputPath(job, new Path(args[0]));
            FileOutputFormat.setOutputPath(job, new Path(args[1])); 
            
            job.waitForCompletion(true);      

    }

}
