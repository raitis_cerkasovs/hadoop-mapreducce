package wordcount.org.main;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


public class Pair implements Writable,WritableComparable<Pair> {
  
    private Text word;
    private Text next;
    
    public String toString() {
        return word+" : "+next;
    }
    
    // constructors
    
    public Pair(Text word, Text next) {
        this.word = word;
        this.next = next;
    }

    public Pair(String word, String next) {
        this(new Text(word),new Text(next));
    }

    public Pair() {
        this.word = new Text();
        this.next = new Text();
    }
    	
	// get / set
	
	public void setNext(String next){
        this.next.set(next);
    }
	
    public Text getNext() {
        return next;
    }
	
    public void setWord(String word){
       this.word.set(word);
    }

    public Text getWord() {
       return word;
   }
    
    ///////////////////////////////// override

    // sort words in a way that total amount is comming on top
    @Override
    public int compareTo(Pair other) {
        int returnVal = this.word.compareTo(other.getWord());
        if(returnVal != 0){
            return returnVal;
        }
        if(this.next.toString().equals("*")){
            return -1;
        }else if(other.getNext().toString().equals("*")){
            return 1;
        }
        return this.next.compareTo(other.getNext());
    }

    public static Pair read(DataInput in) throws IOException {
        Pair wordPair = new Pair();
        wordPair.readFields(in);
        return wordPair;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        word.write(out);
        next.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        word.readFields(in);
        next.readFields(in);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair wordPair = (Pair) o;

        if (next != null ? !next.equals(wordPair.next) : wordPair.next != null) return false;
        if (word != null ? !word.equals(wordPair.word) : wordPair.word != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = word != null ? word.hashCode() : 0;
        result = 163 * result + (next != null ? next.hashCode() : 0);
        return result;
    }




}
