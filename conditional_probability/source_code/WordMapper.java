package wordcount.org.main;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

public class WordMapper extends Mapper<LongWritable, Text, Pair, IntWritable> {
 
	private Pair pair = new Pair();    
	private IntWritable ONE = new IntWritable(1);
	private IntWritable sum = new IntWritable();
	private static String paragraph = "";


    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
 	
            // if not empty line (end of paragraph) add line to string
  	    	if (value.getLength() > 0) {
  	    		paragraph += " "+value;
  	    	}
  	    	else {
  	    		// split string by empty space
  		    	String[] tokens = paragraph.toString().split("\\s+");
  	            for (int i = 0; i < tokens.length; i++) {
  	            	//clean first word
  	                pair.setWord(tokens[i].replaceAll("\\W", ""));
                    //set counters
  	                int start = i;
  	                int end; 
  	                // find last word
  	                if (i + 1 >= tokens.length) {
  	                    end = tokens.length-1;
  	                } else {
  	                	end = i +  1;
  	                }
  	                
  	                
  	                for (int j = start; j <= end; j++) {
  	                    if (j != i) {
  	                    // set and clean second
  	                    pair.setNext(tokens[j].replaceAll("\\W", ""));
  	                    context.write(pair, ONE); }
  	                }
  	               
  	                // Extra element, shows total amount
  	               pair.setNext("*"); 
  	               sum.set(end - start); 
  	               context.write(pair, sum); 

  	            }
  	            paragraph = "";
  	        }
    	
    }
}
