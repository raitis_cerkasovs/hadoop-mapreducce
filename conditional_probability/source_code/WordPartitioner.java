package wordcount.org.main;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;


public class WordPartitioner extends Partitioner<Pair,IntWritable> {

    @Override
    // write hash code only for first word, to get it to the same reducer
    // abs to avoin negative value
    public int getPartition(Pair wordPair, IntWritable intWritable, int numPartitions) {
        return Math.abs(wordPair.getWord().hashCode()) % numPartitions;
    }
}
