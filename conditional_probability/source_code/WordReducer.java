package wordcount.org.main;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class WordReducer extends Reducer<Pair, IntWritable, Pair, DoubleWritable> {
    private DoubleWritable sum = new DoubleWritable();
    private DoubleWritable wordCount = new DoubleWritable();
    private Text currentWord = new Text("NOT_SET");
    private Text flag = new Text("*");

    
    protected void reduce(Pair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
       
    	if (key.getNext().equals(flag)) {
            if (key.getWord().equals(currentWord)) {
                sum.set(sum.get() + getTotalCount(values));
            } else {
                currentWord.set(key.getWord());
                sum.set(0);
                sum.set(getTotalCount(values));
            }
        } else {
            int count = getTotalCount(values);
            wordCount.set((double) count / sum.get());
            context.write(key, wordCount);
        }

    }

    private int getTotalCount(Iterable<IntWritable> values) {
        int count = 0;
        for (IntWritable value : values) {
            count += value.get();
        }
        return count;
    }
}
