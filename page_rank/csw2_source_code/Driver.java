package wordcount.org.main;

import java.io.IOException;
import java.util.Date;
import java.util.Formatter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class Driver {


      public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
    	  
              //////////////////////////// first part
    	      // build adjacency list and give initial page rank 1
    	
      	      Configuration conf1 = new Configuration();      
              conf1.setBoolean("mapred.output.compress", false);

              @SuppressWarnings("deprecation")
  			  Job job1 = new Job(conf1, "csw2");
              job1.setJarByClass(Driver.class);

              job1.setMapOutputKeyClass(Text.class);
              job1.setMapOutputValueClass(Text.class);
            
              job1.setMapperClass(FirstMapper.class);
              job1.setReducerClass(FirstReducer.class);
            
              job1.setInputFormatClass(TextInputFormat.class);
              job1.setOutputFormatClass(TextOutputFormat.class);
 
              //run locally
              //@SuppressWarnings("resource")
              //Formatter formatter = new Formatter();
              //String outpath = "Out"
              //+ formatter.format("%1$tm%1$td%1$tH%1$tM%1$tS", new Date());
              //FileInputFormat.setInputPaths(job1, new Path("In"));
              //FileOutputFormat.setOutputPath(job1, new Path(outpath));
              
              FileInputFormat.addInputPath(job1, new Path(args[0]));
              FileOutputFormat.setOutputPath(job1, new Path(args[1])); 
              
              job1.waitForCompletion(true);
              
              //////////////////////////// second part
              // 40 iterations
              
              String inpath = args[1];
              for (int i=0; i<=40;i++) {
          		          		
          	      Configuration conf2 = new Configuration();      
                  conf2.setBoolean("mapred.output.compress", false);

                  @SuppressWarnings("deprecation")
      			  Job job2 = new Job(conf2, "csw2");
                  job2.setJarByClass(Driver.class);
                  
                  job2.setOutputKeyClass(LongWritable.class);
                  job2.setOutputValueClass(Text.class);

                  job2.setMapOutputKeyClass(Text.class);
                  job2.setMapOutputValueClass(Text.class);
                
                  job2.setMapperClass(SecondMapper.class);
                  job2.setReducerClass(SecondReducer.class);
                
                  job2.setInputFormatClass(TextInputFormat.class);
                  job2.setOutputFormatClass(TextOutputFormat.class);

                  //local run
                  //@SuppressWarnings("resource")
                  //Formatter formatter2 = new Formatter();
                  //String outpath2 = "Out"
                  //+ formatter2.format("%1$tm%1$td%1$tH%1$tM%1$tS", new Date());
                  //FileInputFormat.setInputPaths(job2, new Path(inpath));
                  //FileOutputFormat.setOutputPath(job2, new Path(outpath2));
                  
                  FileInputFormat.addInputPath(job2, new Path(inpath));
                  FileOutputFormat.setOutputPath(job2, new Path(args[1] + "iteration" + i)); 
                  
                  job2.waitForCompletion(true);
                  
                  inpath = args[1] + "iteration" + i;
          	}

      }

} 
