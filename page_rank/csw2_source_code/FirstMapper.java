package wordcount.org.main;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class FirstMapper extends Mapper<LongWritable, Text, Text, Text> {

    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] nodes = value.toString().split("	");
        Text node = new Text(nodes[0].trim());
        Text link  = new Text(nodes[1].trim());

        context.write(node, link);   	
   }

}

