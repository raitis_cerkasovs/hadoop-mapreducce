package wordcount.org.main;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.Text;

public class FirstReducer extends Reducer<Text, Text, Text, Text> {

	ArrayList<Text> adjacencyList = new ArrayList<Text>(); // adjacency list

    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException  {

            adjacencyList.clear();
            while (values.iterator().hasNext())
            {    	
              adjacencyList.add(new Text(((Text) values.iterator().next()).toString()));
            }
            
            // initial page rank
            String initPr = "1";
            // links
            String links = "links:"+ adjacencyList.toString();
            
            Text node = new Text(key + "");
            Text val  = new Text(initPr + " " + links);
            
            context.write(node, val);
    }
    
}
