package wordcount.org.main;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class SecondMapper extends Mapper<LongWritable, Text, Text, Text>
{
	
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

                String[] line = value.toString().split("\t");
                String node = line[0];

                String[] adjacencyList = line[1].split(" links:");
                String[] records = adjacencyList[1].substring(1,adjacencyList[1].length() - 1).split(", ");


                double currentPr = Double.parseDouble(adjacencyList[0]);
                currentPr /= records.length;
                context.write(new Text(node), new Text(currentPr + " links:" + adjacencyList[1]));


                for (int i = 0; i < records.length; i++)
                {
                	Text recordsT = new Text(records[i]);
                	Text pageRank = new Text(currentPr + "");
                    context.write(recordsT, pageRank);
                }
        }
}

