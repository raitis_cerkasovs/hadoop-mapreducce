package wordcount.org.main;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class SecondReducer extends Reducer<Text, Text, Text, Text>
{
  long nodesSum = 75879; // from file
 
  public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException  {

        	
                double pageRankSum = 0;
                String links = "";
                while (values.iterator().hasNext())
                {
                        Text value = (Text) values.iterator().next();
                        if (value.toString().contains(" links:"))
                        {
                                String[] data = value.toString().split(" links:");
                                links = data[1];
                        }
                        else
                        {
                                pageRankSum += Double.parseDouble(value.toString());
                        }
                }
                 
                pageRankSum = 0.1/nodesSum + 0.9*pageRankSum;
                
                Text node = new Text(key + "");

                // check for dead end
                if (links.length() >= 1) {
                	Text result = new Text(pageRankSum + " links:" + links);
                    context.write(node,result);
                }
                else if (key.toString().compareTo("") != 0 && key != null) {
                	Text result = new Text(pageRankSum + " links:[]");
                    context.write(node,result);
                }
        }
}

 
 


 

